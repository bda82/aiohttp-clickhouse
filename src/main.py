import os
from aiohttp.web import Response, Request, Application, run_app, get

from clickhouse_driver import Client

host = 'localhost'
port = 8123

CLICKHOUSE_DB = os.environ.get('CLICKHOUSE_DB',
                               'clickhouse')
CLICKHOUSE_USER = os.environ.get('CLICKHOUSE_USER',
                                 'root')
CLICKHOUSE_PASSWORD = os.environ.get('CLICKHOUSE_PASSWORD',
                                     'password')

client_name = 'clickhouse'

client = Client(
    host=host,
    port=port,
    client_name=client_name,
    user=CLICKHOUSE_USER,
    secret=CLICKHOUSE_PASSWORD,
    verify=False,
    database=CLICKHOUSE_DB,
    compression=True
)


async def show_tables():
    tables = await client.execute('SHOW TABLES')
    return tables


async def create_table():
    table = await client.execute('CRETAE TABLE notes (title String, description String) ENGINE = Memory')
    return table


async def select_all():
    selection = await client.execute('SELECT title, description FROM notes');
    return selection


async def select_title(title: str):
    selection = await client.execute(f'SELECT title, description FROM notes WHERE title = {title}')
    return selection


async def insert(title: str, description: str):
    insertion = await client.execute(
        f'INSERT INTO notes (title, description) VALUES',
        [(title, description)]
    )


async def index(request: Request) -> Response:
    return Response(text="index")


async def tables(request: Request) -> Response:
    tables = show_tables()
    print(tables)
    return Response(text="tables")


app = Application()
app.add_routes([
    get("/", index, name="index"),
])

run_app(app)
