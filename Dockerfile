FROM python:3.8

WORKDIR /usr/src/aiohttp

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY requirements.txt /usr/src/aiohttp/requirements.txt

RUN pip install -U pip --no-cache-dir && \
	pip install --no-cache-dir wheel && \
	pip install --no-cache-dir -r requirements.txt

COPY entrypoint.sh /usr/src/aiohttp/entrypoint.sh

COPY ./src /usr/src/aiohttp/src

ENTRYPOINT ["/usr/src/aiohttp/entrypoint.sh"]
